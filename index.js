const CONSTANTS = {
    AUTH_TOKEN_FIELD_NAME: "authentication_token",
    AUTH_TOKEN_HEADER_NAME: "Auth-Token",

    DEFAULT_DATE_FORMAT: "DD.MM.YYYY",
    DEFAULT_TIME_FORMAT: "HH:mm",

    UTC_FORMAT: "YYYY-MM-DDTHH:mm:ssZZ",
    UTC_BEGIN_DAY_FORMAT: "YYYY-MM-DDT00:00:00ZZ",
    UTC_END_DAY_FORMAT: "YYYY-MM-DDT23:59:59ZZ",

    YA_METRIKA_ID: 45807066
};

function injectConstants(angularModule) {
    for(let key in CONSTANTS) {
        angularModule.constant(key, CONSTANTS[key]);
    }
}


module.exports = {
    CONSTANTS,
    injectConstants
};